package com.example.countryregionapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.countryregionapi.models.Region;

@Service
public class RegionService {
    public ArrayList<Region> getVietnameseRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region("hn", "Hà Nội"));
        regions.add(new Region("hcm", "Hồ Chí Minh"));
        regions.add(new Region("hp", "Hải Phòng"));

        return regions;
    }

    public ArrayList<Region> getChineseRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region("bk", "Bắc Kinh"));
        regions.add(new Region("nk", "Nam Kinh"));
        regions.add(new Region("qc", "Quảng Châu"));

        return regions;
    }

    public ArrayList<Region> getUsaRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region("ny", "New York"));
        regions.add(new Region("as", "Alaska"));
        regions.add(new Region("fl", "Florida"));

        return regions;
    }

    public ArrayList<Region> getThaiRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region("bak", "BankKok"));
        regions.add(new Region("sin", "Sinha"));
        regions.add(new Region("tn", "Tin"));

        return regions;
    }

    public ArrayList<Region> getCampodiaRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region("nog", "NongBenh"));
        regions.add(new Region("hin", "Hinla"));
        regions.add(new Region("kro", "krokmen"));

        return regions;
    }

    public ArrayList<Region> getLaosRegions() {
        ArrayList<Region> regions = new ArrayList<>();
        regions.add(new Region("ad", "Adom"));
        regions.add(new Region("bon", "Bongsi"));
        regions.add(new Region("kan", "Kante"));

        return regions;
    }

    public Region getRegionByRegionCode(String regionCode) {
        ArrayList<Region> regions = new ArrayList<>();
        regions.addAll(getVietnameseRegions());
        regions.addAll(getChineseRegions());
        regions.addAll(getUsaRegions());
        Region result = new Region();

        for (Region region : regions) {
            if (region.getRegionCode().equalsIgnoreCase(regionCode)) {
                result = region;
                break;
            }
        }

        return result;
    }
}
