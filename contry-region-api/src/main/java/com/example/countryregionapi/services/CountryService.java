package com.example.countryregionapi.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.countryregionapi.models.Country;

@Service
public class CountryService {
    @Autowired
    RegionService regionService;

    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> countries = new ArrayList<>();
        Country vietnam = new Country("vn", "Việt Nam", regionService.getVietnameseRegions());
        Country china = new Country("cn", "China", regionService.getChineseRegions());
        Country usa = new Country("us", "America", regionService.getUsaRegions());
        Country thailand = new Country("thai", "Thailand", regionService.getThaiRegions());
        Country campodia = new Country("cam", "Campodia", regionService.getCampodiaRegions());
        Country laos = new Country("lao", "Laos", regionService.getLaosRegions());
        countries.addAll(Arrays.asList(vietnam, usa, china, thailand, campodia, laos));

        return countries;
    }

    public Country getCountryByCountryCode(String countryCode) {
        ArrayList<Country> countries = getAllCountries();
        Country result = new Country();

        for (Country country : countries) {
            if (country.getCountryCode().equalsIgnoreCase(countryCode)) {
                result = country;
                break;
            }
        }

        return result;
    }

    public Country getCountryByIndeX(int idx) {
        ArrayList<Country> countries = getAllCountries();

        for (int index = 0; index < countries.size(); index++) {
            if (index >= 0 && index < countries.size()) {
                return countries.get(index);
            }
        }
        return null;
    }
}
