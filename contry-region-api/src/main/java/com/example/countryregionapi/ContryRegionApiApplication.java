package com.example.countryregionapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContryRegionApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContryRegionApiApplication.class, args);
	}

}
