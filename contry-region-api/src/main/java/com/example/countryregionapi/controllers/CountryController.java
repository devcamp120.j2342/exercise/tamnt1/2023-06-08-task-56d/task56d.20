package com.example.countryregionapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

import com.example.countryregionapi.models.Country;
import com.example.countryregionapi.services.CountryService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CountryController {
    @Autowired
    CountryService countryService;

    @GetMapping(value = "/countries")
    public ArrayList<Country> getAllCountries() {
        ArrayList<Country> countries = countryService.getAllCountries();

        return countries;
    }

    @GetMapping(value = "/country-info")
    public Country getMethodName(@RequestParam(value = "code", defaultValue = "vn") String countryCode) {
        return countryService.getCountryByCountryCode(countryCode);
    }

    @GetMapping(value = "/countries/{index}")
    public Country getMethodName(@PathVariable("index") int index) {
        return countryService.getCountryByIndeX(index);

    }
}
